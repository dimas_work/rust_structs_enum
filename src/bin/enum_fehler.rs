use fehler::*;
use std::fmt::Debug;

#[derive(Debug, Clone)]
pub enum MyErrorKind {
    NotFound,
}

#[derive(Debug)]
pub struct Error {
    error_kind: MyErrorKind,
}

impl Error {
    fn new() -> Error {
        Error {
            error_kind: MyErrorKind::NotFound,
        }
    }
}

impl Error {
    fn kind(&self) -> MyErrorKind {
        self.error_kind.clone()
    }
}

#[throws(_)]
fn do_it() -> i32 {
    let e = Error::new();
    if true {
        println!("ref : {:?}", e.kind());
        throw!(e);
    }

    0
}

#[throws(_)]
fn main() {
    do_it()?;
}
