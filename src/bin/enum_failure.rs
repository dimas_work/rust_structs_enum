use std::io;
use std::io::BufRead;

use failure::{Backtrace, Context, Error, Fail};

use std::fmt::{self, Display};

#[derive(Copy, Clone, Eq, PartialEq, Debug, Fail)]
enum MyErrorKind {
    // A plain enum with no data in any of its variants
    //
    // For example:
    #[fail(display = "A contextual error message.")]
    OneVariant,
    // ...
}

impl Fail for MyError {
    fn cause(&self) -> Option<&dyn Fail> {
        self.inner.cause()
    }

    fn backtrace(&self) -> Option<&Backtrace> {
        self.inner.backtrace()
    }
}

impl Display for MyError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        Display::fmt(&self.inner, f)
    }
}

impl MyError {
    /*
    pub fn kind(&self) -> MyErrorKind {
        *self.inner.get_context()
    }*/

    pub fn into_failure(&self) -> failure::Error {
        let s: String = self.inner.to_string();
        failure::format_err!("{:?}", s.clone())
    }
}

impl From<MyErrorKind> for MyError {
    fn from(kind: MyErrorKind) -> MyError {
        MyError {
            inner: Context::new(kind),
        }
    }
}

impl From<Context<MyErrorKind>> for MyError {
    fn from(inner: Context<MyErrorKind>) -> MyError {
        MyError { inner: inner }
    }
}

#[derive(Debug)]
struct MyError {
    inner: Context<MyErrorKind>,
}

fn my_function() -> Result<(), Error> {
    let stdin = io::stdin();

    for line in stdin.lock().lines() {
        let line = line?;

        if line.chars().all(|c| c.is_whitespace()) {
            break;
        }

        if !line.starts_with("$") {
            return Err(failure::format_err!("Input did not begin with `$`"));
        } else if !line.starts_with("@") {
            let e = MyError {
                inner: Context::new(MyErrorKind::OneVariant),
            };
            return Err(e.into_failure());
        }

        println!("{}", &line[1..]);
    }

    Ok(())
}

fn main() {
    println!("Use the Error type function!");
    assert!(my_function().is_ok(), true);
}
