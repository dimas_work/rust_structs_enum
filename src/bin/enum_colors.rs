enum Colors {
    Blue,
    Green,
    Red,
    Yellow,
    Grey,
    Brown,
}

fn translate_ccase(clr: Colors) -> String {
    match clr {
        Colors::Blue => "Blue".to_string(),
        Colors::Green => "Green".to_string(),
        Colors::Red => "Red".to_string(),
        Colors::Yellow => "Yellow".to_string(),
        Colors::Grey => "Grey".to_string(),
        Colors::Brown => "Brown".to_string(),
    }
}

fn main() {
    println!("Debug enums!");
    println!("This is blue  : {:?}", translate_ccase(Colors::Blue));
    println!("This is green : {:?}", translate_ccase(Colors::Green));
    println!("This is red   : {:?}", translate_ccase(Colors::Red));
    println!("This is yellow: {:?}", translate_ccase(Colors::Yellow));
    println!("This is grey  : {:?}", translate_ccase(Colors::Grey));
    println!("This is brown : {:?}", translate_ccase(Colors::Brown));
}
