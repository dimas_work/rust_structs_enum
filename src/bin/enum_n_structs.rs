use std::fmt::Debug;

#[derive(Debug,Clone)]
pub enum MyErrorKind {
    NotFound,
    PermissionDenied,
    ConnectionRefused,
    ConnectionReset,
    ConnectionAborted,
    NotConnected,
    AddrInUse,
    AddrNotAvailable,
    BrokenPipe,
    AlreadyExists,
    WouldBlock,
    InvalidInput,
    InvalidData,
    TimedOut,
    WriteZero,
    Interrupted,
    Other,
    UnexpectedEof,
}

#[derive(Debug, Clone)]
struct MyError {
    error_kind : MyErrorKind,
}

impl MyError {
    fn new() -> MyError {
        MyError { error_kind: MyErrorKind::NotFound }
    }
}

impl MyError {
    fn kind(&self) -> MyErrorKind {
        self.error_kind.clone()
    }
}

fn main() {
    println!("Hello, Error Kind Disect!");
    let e = MyError::new();
    println!("{:?}", e.kind());
}
