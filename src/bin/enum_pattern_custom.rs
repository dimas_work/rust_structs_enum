use std::collections::HashMap;
use std::fmt::{self, Debug, Display};
use std::thread::sleep;
use std::time::{Duration, Instant};

use systemstat::{data::ByteSize, saturating_sub_bytes};

#[derive(Debug, Clone, PartialEq, Eq)]
pub enum PropertyValue {
    NotGiven(String),
    Degrees(u64),
    Bytes(ByteSize),
}

impl Display for PropertyValue {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &*self {
            PropertyValue::NotGiven(s) => write!(f, "{}", s),
            PropertyValue::Degrees(n) => write!(f, "{}", n.to_string()),
            PropertyValue::Bytes(bs) => write!(f, "{}", bs.to_string()),
        }
    }
}

impl PropertyValue {
    fn compare_bytes(&self, s: ByteSize) -> Option<ByteSize> {
        match &*self {
            PropertyValue::Bytes(bs) => {
                let bs = bs.clone();
                if bs > s {
                    Some(saturating_sub_bytes(bs, s))
                } else {
                    Some(saturating_sub_bytes(s, bs))
                }
            }
            _ => None,
        }
    }
}

fn main() {
    println!("calculation with property (time, etc) enum!");
    let now = Instant::now();
    let mut hash: HashMap<String, PropertyValue> = HashMap::new();
    let ipv4 = PropertyValue::NotGiven("192.168.178".to_string());
    let size = PropertyValue::Bytes(ByteSize::b(199));
    let compare = ByteSize::b(1000);
    hash.insert("chip".to_string(), size);
    hash.insert("ipv4".to_string(), ipv4);

    sleep(Duration::new(2, 0));

    println!("{}", now.elapsed().as_secs());
    println!("data : {:?}", hash);

    let get_chip = hash.get(&("chip".to_string()));
    let get_chip = get_chip.unwrap();

    println!("chip mem: {}", get_chip.clone());

    let compared = get_chip.compare_bytes(compare);
    println!("mem compared: {:?}", &compared);

    hash.insert("chip".to_string(), PropertyValue::Bytes(compared.unwrap()));
    println!("data : {:?}", hash);
}
